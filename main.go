package main

import (
	"log"
	"net/http"
	"text/template"

	users "gitlab.com/olzemal/login-go/users"
)

func route(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/signup-form":
		fillTemplateToWriter(w, "templates/sign-up.html.tmpl", nil)
	case "/login-form":
		fillTemplateToWriter(w, "templates/login.html.tmpl", nil)
	case "/signup":
		signUpUser(w, r)
	case "/login":
		loginUser(w, r)
	}
}

func fillTemplateToWriter(w http.ResponseWriter, file string, data interface{}) {
	t, _ := template.ParseFiles(file)
	err := t.ExecuteTemplate(w, t.Name(), data)
	if err != nil {
		log.Printf("Error Filling Template %s: %s", file, err.Error())
	}
}

func signUpUser(w http.ResponseWriter, r *http.Request) {
	user := users.User{
		Username: r.FormValue("user"),
		Password: r.FormValue("password"),
	}
	usvc := users.UserService{}
	err := usvc.CreateUser(user)

	var message string
	switch err {
	case nil:
		message = "Successfully signed up"
		log.Printf("User \"%s\" created", user.Username)
	default:
		message = err.Error()
		log.Printf("Failed signup attempt with user: \"%s\"", user.Username)
	}
	fillTemplateToWriter(w, "templates/sign-up.html.tmpl", message)
}

func loginUser(w http.ResponseWriter, r *http.Request) {
	user := users.User{
		Username: r.FormValue("user"),
		Password: r.FormValue("password"),
	}
	usvc := users.UserService{}
	success := usvc.VerifyUser(user)
	if success {
		fillTemplateToWriter(
			w,
			"templates/secret-content.html.tmpl",
			"Signed in as "+user.Username)
		log.Printf("\"%s\" signed in successfully", user.Username)
	} else {
		fillTemplateToWriter(
			w,
			"templates/login.html.tmpl",
			"Wrong Username or Password")
		log.Printf("Failed login attempt for \"%s\"", user.Username)
	}
}

func main() {
	http.HandleFunc("/", route)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
