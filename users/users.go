package users

import (
	"errors"

	bcrypt "golang.org/x/crypto/bcrypt"
)

type User struct {
	Username string
	Password string
}

type AuthUser struct {
	Username string
	Password string
}

type UserService struct {
	// DBConnector string
}

// Placeholder for a Database User-Table
var authUsers = map[string]AuthUser{}

func (UserService) CreateUser(user User) error {
	// Check if User already Exists
	_, exists := authUsers[user.Username]
	if exists {
		return errors.New("User already exists")
	}

	// Check if password is compliant with the password policy
	// TODO

	// Create new authUser and insert into the Database
	pwHash, err := bcrypt.GenerateFromPassword([]byte(user.Password), 0)
	if err != nil {
		return err
	}
	authUser := AuthUser{
		Username: user.Username,
		Password: string(pwHash),
	}
	authUsers[user.Username] = authUser
	return nil
}

func (UserService) VerifyUser(user User) bool {
	authUser, exists := authUsers[user.Username]
	if !exists {
		return false
	}

	err := bcrypt.CompareHashAndPassword(
		[]byte(authUser.Password),
		[]byte(user.Password))
	return err == nil
}
